//
//  UserDefaults+Ext.swift
//  pokedex
//
//  Created by Wesley Calazans on 18/08/23.
//

import Foundation

extension UserDefaults {
    
    public enum Keys {
        static let specie = "kSpecie"
    }
    
    var specie: String {
        get { return string(forKey: Keys.specie) ?? "" }
        set {
            set(newValue, forKey: Keys.specie)
            _ = synchronize()
        }
    }
}
