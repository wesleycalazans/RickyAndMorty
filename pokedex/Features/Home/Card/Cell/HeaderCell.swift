//
//  HeaderCell.swift
//  pokedex
//
//  Created by Wesley Calazans on 17/08/23.
//

import UIKit
import DSM

class TitleSupplementaryView: UICollectionReusableView {
    
    static let identifier = "TitleSupplementaryView"
    
    lazy var sectionLabel = LabelDefault(text: "", font: .systemFont(ofSize: 23, weight: .bold))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
}

extension TitleSupplementaryView {
    
    func configure() {
        addSubview(sectionLabel)
        let inset = CGFloat(10)
        
        NSLayoutConstraint.activate([
            sectionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: inset),
            sectionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -inset),
            sectionLabel.topAnchor.constraint(equalTo: topAnchor, constant: inset),
            sectionLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -inset)
        ])
    }
    
    func configureHeader(data: String) {
        sectionLabel.text = data
    }
    
}
