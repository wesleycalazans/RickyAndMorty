//
//  CardCell.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit
import StackViewKit
import SDWebImage
import DSM

class CardCell: UICollectionViewCell {
    
    static let identifier = "CardCell"
    
    lazy var characterImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        return image
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.text = "Ricky smith"
        label.font = .systemFont(ofSize: 21, weight: .bold)
        return label
    }()
    
    lazy var statusBagde: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.backgroundColor = .green
        return view
    }()
    
    lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.text = "Alive - Human"
        label.font = .systemFont(ofSize: 13, weight: .bold)
        return label
    }()
    
    lazy var lastLocationTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .systemGray
        label.text = "Last known location:"
        label.font = .systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    lazy var lastLocationNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.text = "Earth (Evil Rick's Target Dimension)"
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14, weight: .medium)
        return label
    }()
    
    lazy var firstSeenTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .systemGray
        label.text = "First seen in:"
        label.font = .systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    lazy var firstSeenEpisodeNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.text = "Close Rick-counters of the Rick Kind"
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14, weight: .medium)
        return label
    }()
    
    lazy var statusView: HStackView = {
        let view = HStackView(alignment: .center,
                              spacing: 8,
                              subviews: [
                                statusBagde,
                                statusLabel
                              ]
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var cardView: CardView = {
        let view = CardView(
            topView: VStackView(subviews: [
                characterImage
            ]),
            bottomView: VStackView(alignment: .fill,
                                   spacing: 6,
                                   margins: .init(top: 16, left: 16, bottom: 16, right: 16),
                                   subviews: [
                                    titleLabel,
                                    statusView,
                                    lastLocationTitleLabel,
                                    lastLocationNameLabel,
                                    firstSeenTitleLabel,
                                    firstSeenEpisodeNameLabel
                                   ])
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViewCode()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initViewCode()
    }
    
}

// MARK: - Functions

extension CardCell {
    
    func configureCardCell(data: ResultModel) {
        titleLabel.text = data.name
        characterImage.sd_setImage(with: URL(string: data.image ?? ""))
        statusLabel.text = "\(data.status ?? "") - \(data.species ?? "")"
        lastLocationNameLabel.text = data.location?.name

        switch data.status {
        case "Alive":
            statusBagde.backgroundColor = .systemGreen
        case "Dead":
            statusBagde.backgroundColor = .systemRed
        case "Unknown":
            statusBagde.backgroundColor = .systemGray
        default:
            statusBagde.backgroundColor = .systemGray
        }
    }
    
}

// MARK: -  ConfigureConstraints

extension CardCell {
    
    func initViewCode() {
        configureSubviews()
        configureConstraints()
        configureAdditionalBehaviors()
    }
    
    func configureSubviews() {
        contentView.addSubview(cardView)
    }
    
    func configureConstraints() {
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: contentView.topAnchor),
            cardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            statusBagde.widthAnchor.constraint(equalToConstant: 10),
            statusBagde.heightAnchor.constraint(equalToConstant: 10)
        ])
    }
    
    func configureAdditionalBehaviors() { }
    
}
