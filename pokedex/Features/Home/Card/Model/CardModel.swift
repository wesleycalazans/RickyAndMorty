//
//  CardModel.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import Foundation

// MARK: - DataModel

struct DataModel: Codable {
    var info: Info?
    var results: [ResultModel]
}

// MARK: - Info

struct Info: Codable {
    var count, pages: Int?
    var next: String?
    var prev: String?
}

// MARK: - Result

struct ResultModel: Codable {
    var id: Int?
    var name: String?
    var status: String?
    var species: String?
    var type: String?
    var gender: String?
    var origin, location: Location?
    var image: String?
    var episode: [String]?
    var url: String?
    var created: String?
    
    var uniqueSpecies: [String]?
}

// MARK: - Location

struct Location: Codable {
    var name: String?
    var url: String?
}

struct EpisodeModel: Codable {
    var id: Int?
    var name: String?
    var airDate: String?
    var episode: String?
    var characters: [String]?
    var url: String?
    var created: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, episode, characters, url, created
        case airDate = "air_date"
    }
}
