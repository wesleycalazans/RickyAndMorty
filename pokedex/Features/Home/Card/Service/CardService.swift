//
//  CardService.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit
import ServiceKit

enum Base {
    static let url = "https://rickandmortyapi.com/api/"
}

class CardService: UIView {
    
    func getCharacterList(specie: String, completion: @escaping (Result<[ResultModel], NetworkError>) -> Void) {
        let urlString: String = Base.url + "character?page=3&species=\(specie)"
        let endpoint = Endpoint(url: urlString)
        
        ServiceManager.shared.request(with: endpoint, decodeType: DataModel.self) { result in
            switch result {
            case .success(let success):
                completion(.success(success.results))
            case .failure(let failure):
                completion(.failure(failure))
            }
        }
    }
    
    func getCharacterMythologicalList(specie: String, completion: @escaping (Result<[ResultModel], NetworkError>) -> Void) {
        let urlString: String = Base.url + "character?page=3&species=\(specie)"
        let endpoint = Endpoint(url: urlString)
        
        ServiceManager.shared.request(with: endpoint, decodeType: DataModel.self) { result in
            switch result {
            case .success(let success):
                completion(.success(success.results))
            case .failure(let failure):
                completion(.failure(failure))
            }
        }
    }
    
    func getUniqueSpeciesList(completion: @escaping (Result<Set<String>, NetworkError>) -> Void) {
        let urlString: String = Base.url + "character?page=1"
        let endpoint = Endpoint(url: urlString)
        
        ServiceManager.shared.request(with: endpoint, decodeType: DataModel.self) { result in
            switch result {
            case .success(let success):
                
                let speciesSet = Set(success.results.compactMap { $0.species })
                
                completion(.success(speciesSet))
            case .failure(let failure):
                completion(.failure(failure))
            }
        }
    }

}
