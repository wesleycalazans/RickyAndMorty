//
//  CardViewModel.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit

protocol CardViewModelProtocol: AnyObject {
    func cardSuccess()
    func cardError(message: String)
}

class CardViewModel: NSObject {
    
    private weak var delegate: CardViewModelProtocol?
    
    private var service: CardService = CardService()
    private var uniqueSpecies: Set<String> = []
    
    private var humans: [ResultModel] = []
    private var aliens: [ResultModel] = []
    private var humanoids: [ResultModel] = []
    private var mythological: [ResultModel] = []
    
}

// MARK: - Functions

extension CardViewModel {
    
    func delegate(delegate: CardViewModelProtocol?) {
        self.delegate = delegate
    }
    
    func fetchUniqueSpecies() {
        service.getUniqueSpeciesList { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let specie):
                uniqueSpecies = specie
                delegate?.cardSuccess()
            case .failure(let failure):
                delegate?.cardError(message: failure.errorDescription ?? "")
            }
        }
    }
    
    func loadSpecies(indexPath: IndexPath) -> String {
        let sortedSpecies = uniqueSpecies.sorted()
        guard indexPath.row < sortedSpecies.count else { return "" }
        return sortedSpecies[indexPath.row]
    }
    
}

// MARK: - Humans Functions

extension CardViewModel {
    
    var humansNumberOfItemsInSection: Int {
        return humans.count
    }
    
    func fetchCharactersForHumans() {
        service.getCharacterList(specie: "human") { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let specie):
                humans = specie
            case .failure(let failure):
                delegate?.cardError(message: failure.errorDescription ?? "")
            }
        }
    }
    
    func loadHumans(indexPath: IndexPath) -> ResultModel {
        return humans[indexPath.row]
    }
    
}

// MARK: - Aliens Functions

extension CardViewModel {
    
    var numberOfSections: Int {
        return 4
    }
    
    var aliensNumberOfItemsInSection: Int {
        return aliens.count
    }
    
    func fetchCharactersForAliens() {
        service.getCharacterList(specie: "alien") { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let characters):
                aliens = characters
                delegate?.cardSuccess()
            case .failure(let failure):
                delegate?.cardError(message: failure.errorDescription ?? "")
            }
        }
    }
    
    func loadAliens(indexPath: IndexPath) -> ResultModel {
        return aliens[indexPath.row]
    }
    
}

// MARK: - Humans Functions

extension CardViewModel {
    
    var humanoidsNumberOfItemsInSection: Int {
        return humanoids.count
    }
    
    func fetchCharactersForHumanoids() {
        service.getCharacterList(specie: "humanoid") { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let specie):
                humanoids = specie
            case .failure(let failure):
                delegate?.cardError(message: failure.errorDescription ?? "")
            }
        }
    }
    
    func loadHumanoids(indexPath: IndexPath) -> ResultModel {
        return humanoids[indexPath.row]
    }
    
}

// MARK: - Mythological Functions

extension CardViewModel {
    
    var mythologicalNumberOfItemsInSection: Int {
        return mythological.count
    }
    
    func fetchCharactersForMythological() {
        service.getCharacterMythologicalList(specie: "mythological") { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let specie):
                mythological = specie
            case .failure(let failure):
                delegate?.cardError(message: failure.errorDescription ?? "")
            }
        }
    }
    
    func loadMythological(indexPath: IndexPath) -> ResultModel {
        return mythological[indexPath.row]
    }
    
}

extension CardViewModel {
    
    enum SectionType: Int, CaseIterable {
        case humans, aliens, humanoids, mythological
        
        var title: String {
            switch self {
            case .humans: return "Humans"
            case .aliens: return "Aliens"
            case .humanoids: return "Humanoids"
            case .mythological: return "Mythological Creatures"
            }
        }
    }
    
    func configureCell(collectionView: UICollectionView, for indexPath: IndexPath) -> CardCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardCell.identifier, for: indexPath) as? CardCell else { fatalError("Could not dequeue CardCell") }
        
        let data: ResultModel
        
        switch indexPath.section {
        case 0:
            data = loadHumans(indexPath: indexPath)
        case 1:
            data = loadAliens(indexPath: indexPath)
        case 2:
            data = loadHumanoids(indexPath: indexPath)
        case 3:
            data = loadMythological(indexPath: indexPath)
        default:
            return .init()
        }
        
        cell.configureCardCell(data: data)
        return cell
    }
    
    func configureHeader(collectionView: UICollectionView, for indexPath: IndexPath) -> TitleSupplementaryView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: TitleSupplementaryView.identifier, for: indexPath) as? TitleSupplementaryView else {
            return .init()
        }
        
        header.sectionLabel.text = SectionType(rawValue: indexPath.section)?.title ?? ""
        return header
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        switch section {
        case 0:
            return humansNumberOfItemsInSection
        case 1:
            return aliensNumberOfItemsInSection
        case 2:
            return humanoidsNumberOfItemsInSection
        case 3:
            return mythologicalNumberOfItemsInSection
        default:
            return 0
        }
    }
    
}
