//
//  HomeService.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit
import ServiceKit

class HomeService: UIView {
    
    func getSpecieList(completion: @escaping (Result<[ResultModel], NetworkError>) -> Void) {
        let urlString: String = "https://rickandmortyapi.com/api/character?page=1"
        let endpoint = Endpoint(url: urlString)
        
        ServiceManager.shared.request(with: endpoint, decodeType: DataModel.self) { result in
            switch result {
            case .success(let success):
                completion(.success(success.results))
            case .failure(let failure):
                completion(.failure(failure))
                
            }
        }
    }
    
}
