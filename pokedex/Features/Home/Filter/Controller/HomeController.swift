//
//  ViewController.swift
//  pokedex
//
//  Created by Wesley Calazans on 14/08/23.
//

import UIKit
import DSM

class HomeController: UIViewController {
    
    private var homeView: HomeView?
    private var viewModel: HomeViewModel = HomeViewModel()
    private var cardViewModel: CardViewModel = CardViewModel()
    
    override func loadView() {
        homeView = HomeView()
        view = homeView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDelegate()
        getCompositionalLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureAdditionalBehaviors()
    }
    
}

// MARK: - Functions

extension HomeController {
    
    func configureDelegate() {
        cardViewModel.delegate(delegate: self)
        cardViewModel.fetchCharactersForHumans()
        cardViewModel.fetchCharactersForAliens()
        cardViewModel.fetchCharactersForHumanoids()
        cardViewModel.fetchCharactersForMythological()
    }
    
    func configureAdditionalBehaviors() {
        title = "Home"
        navigationController?.navigationBar.prefersLargeTitles = false
        let searchButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .done, target: self, action: nil)
        navigationItem.setRightBarButtonItems([searchButton], animated: true)
    }
    
    func getCompositionalLayout() {
        let layout = UICollectionViewCompositionalLayout { (sectionNumber, env) in
            return LayoutType.reusable.getLayout()
        }
        
        homeView?.collectionView.setCollectionViewLayout(layout, animated: true)
    }
}

// MARK: - CardViewModelProtocol

extension HomeController: CardViewModelProtocol {
    
    func cardSuccess() {
        homeView?.configureCollectionViewDelegate(delegate: self, datasource: self)
        homeView?.collectionView.reloadData()
    }
    
    func cardError(message: String) {
        alert(message: message,
              title: "Ops, tivemos um problema",
              primaryActionTitle: "Ok",
              secondaryActionTitle: "Tentar novamente",
              primaryActionStyle: .cancel,
              secondaryActionStyle: .default,
              primaryActionHandler: { _ in },
              secondaryActionHandler: { _ in
            self.configureDelegate()
        },
              controller: self)
    }
    
}

// MARK: - UICollectionViewDelegate & UICollectionViewDataSource

extension HomeController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return cardViewModel.numberOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardViewModel.numberOfItems(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return cardViewModel.configureCell(collectionView: homeView?.collectionView ?? UICollectionView(), for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return cardViewModel.configureHeader(collectionView: homeView?.collectionView ?? UICollectionView(), for: indexPath)
    }
    
}
