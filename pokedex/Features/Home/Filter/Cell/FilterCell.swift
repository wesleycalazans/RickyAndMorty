//
//  FilterCell.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit
import StackViewKit

class FilterCell: UIView {
    
    //static let identifier = "FilterCell"
    
    lazy var filterButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "line.3.horizontal.decrease")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .systemBackground
        return button
    }()
    
    lazy var HStack: HStackView = {
        let view = HStackView(alignment: .center,
                              spacing: 16,
                              margins: .init(top: 16, left: 16, bottom: 16, right: 16),
                              subviews: [
                                filterButton
                              ]
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 16
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViewCode()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initViewCode()
    }
    
}

// MARK: -  ConfigureConstraints

extension FilterCell {
    
    func initViewCode() {
        configureSubviews()
        configureConstraints()
        configureAdditionalBehaviors()
    }
    
    func configureSubviews() {
        addSubview(HStack)
    }
    
    func configureConstraints() {
        NSLayoutConstraint.activate([
            HStack.topAnchor.constraint(equalTo: topAnchor),
            HStack.leadingAnchor.constraint(equalTo: leadingAnchor),
            HStack.trailingAnchor.constraint(equalTo: trailingAnchor),
            HStack.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            filterButton.widthAnchor.constraint(equalToConstant: 48),
            filterButton.heightAnchor.constraint(equalToConstant: 48),
        ])
    }
    
    func configureAdditionalBehaviors() { }
    
}
