//
//  HomeCell.swift
//  pokedex
//
//  Created by Wesley Calazans on 14/08/23.
//

import UIKit
import PreviewSwiftUIKit
import StackViewKit
import SDWebImage

class HomeCell: UICollectionViewCell {
    
    static let identifier = "HomeCell"
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.adjustsFontSizeToFitWidth = true
        label.font = .systemFont(ofSize: 16, weight: .bold)
        return label
    }()
    
    lazy var iconImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(systemName: "xmark")?.withRenderingMode(.alwaysTemplate)
        image.tintColor = .black
        image.layer.masksToBounds = true
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    lazy var HStack: HStackView = {
        let view = HStackView(alignment: .center,
                              spacing: 16,
                              margins: .init(top: 16, left: 16, bottom: 16, right: 16),
                              subviews: [
                                titleLabel,
                                iconImage
                              ]
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 16
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViewCode()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initViewCode()
    }
    
}

// MARK: - Functions

extension HomeCell {
    
    func configureCell(data: ResultModel) {
        titleLabel.text = data.species
    }
    
}

// MARK: -  ConfigureConstraints

extension HomeCell {
    
    func initViewCode() {
        configureSubviews()
        configureConstraints()
        configureAdditionalBehaviors()
    }
    
    func configureSubviews() {
        contentView.addSubview(HStack)
    }
    
    func configureConstraints() {
        NSLayoutConstraint.activate([
            HStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            HStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            HStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            HStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    func configureAdditionalBehaviors() { }
    
}

// MARK: - Preview

#if canImport(SwiftUI)
import SwiftUI

struct HomeCell_Previews: PreviewProvider {
    static var previews: some View {
        UIViewPreview {
            HomeCell()
        }
        .preferredColorScheme(.dark)
    }
}
#endif
