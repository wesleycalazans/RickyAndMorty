//
//  HomeViewModel.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit

protocol HomeViewModelProtocol: AnyObject {
    func success()
    func error(message: String)
}

class HomeViewModel: NSObject {

    private var service: HomeService = HomeService()
    private var speciesList: [ResultModel] = []
    private weak var delegate: HomeViewModelProtocol?
    
}

// MARK: - Functions

extension HomeViewModel {
    
    func delegate(delegate: HomeViewModelProtocol?) {
        self.delegate = delegate
    }
    
    func fetchRequest() {
        service.getSpecieList { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let success):
                let uniqueSpeciesNames = Array(Set(success.compactMap { $0.species }))
                speciesList = success.filter { uniqueSpeciesNames.contains($0.species ?? "") }
                delegate?.success()
            case .failure(let failure):
                delegate?.error(message: failure.errorDescription ?? "")
            }
        }
    }
    
    var numberOfSections: Int {
        return 2
    }
    
    var numberOfItemsInSection: Int {
        return speciesList.count
    }
    
    func loadSpecies(indexPath: IndexPath) -> ResultModel {
        return speciesList[indexPath.row]
    }
    
}
