//
//  HomeView.swift
//  pokedex
//
//  Created by Wesley Calazans on 14/08/23.
//

import UIKit
import PreviewSwiftUIKit

class HomeView: UIView {
    
    lazy var filterButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "line.3.horizontal.decrease"), for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 16
        return button
    }()
    
    lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8
        layout.scrollDirection = .vertical
        return layout
    }()
    
    lazy var collectionView: UICollectionView = {
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentInsetAdjustmentBehavior = .never
        view.backgroundColor = .clear
        view.register(HomeCell.self, forCellWithReuseIdentifier: HomeCell.identifier)
        view.register(CardCell.self, forCellWithReuseIdentifier: CardCell.identifier)
        view.register(TitleSupplementaryView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: TitleSupplementaryView.identifier)
        view.showsHorizontalScrollIndicator = false
        view.isScrollEnabled = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViewCode()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initViewCode()
    }
    
}

// MARK: - Functions

extension HomeView {
    
    func configureCollectionViewDelegate(delegate: UICollectionViewDelegate,
                                         datasource: UICollectionViewDataSource) {
        collectionView.delegate = delegate
        collectionView.dataSource = datasource
    }
    
}

// MARK: - ConfigureConstraints

extension HomeView {
    
    func initViewCode() {
        configureSubviews()
        configureConstraints()
        configureAdditionalBehaviors()
    }
    
    func configureSubviews() {
        addSubview(collectionView)
    }
    
    func configureConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    func configureAdditionalBehaviors() {
        backgroundColor = .systemBackground
    }
    
}

// MARK: - Preview

#if canImport(SwiftUI)
import SwiftUI

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        UIViewPreview {
            HomeView()
        }
    }
}
#endif
