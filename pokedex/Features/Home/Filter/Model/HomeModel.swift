//
//  HomeModel.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import Foundation

// MARK: - PokemonModel

struct PokemonModel: Codable {
    var count: Int?
    var next: String?
    var previous: String?
    var results: [PokemonInfo]
}

// MARK: - Result

struct PokemonInfo: Codable {
    var name: String?
    var url: String?
}
