//
//  AppLayout.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit

class AppLayout {
    
    static let shared = AppLayout()
    
    private init() {}
    
    func filters() -> NSCollectionLayoutSection {
        let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1),
                      heightDimension: .fractionalHeight(1)))
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .absolute(150),
                      heightDimension: .absolute(48)),
                                                       subitems: [item])
        
        group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 9)
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        
        section.contentInsets.top = 16
        section.contentInsets.leading = 80
        section.contentInsets.trailing = 16
        section.contentInsets.bottom = 16
        
        return section
    }
    
    func reusable(withHeader: Bool) -> NSCollectionLayoutSection {
        let item = NSCollectionLayoutItem(layoutSize:
                .init(widthDimension: .fractionalWidth(1),
                      heightDimension: .fractionalHeight(1)))
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize:
                .init(widthDimension: .absolute(270),
                      heightDimension: .absolute(350)),
                                                       subitems: [item])
        
        group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 9)
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        
        section.contentInsets.leading = 16
        section.contentInsets.trailing = 16
        section.contentInsets.bottom = 16
        
        if withHeader {
            section.boundarySupplementaryItems = [.init(layoutSize:
                    .init(widthDimension: .fractionalWidth(1),
                          heightDimension: .absolute(60)), elementKind: UICollectionView.elementKindSectionHeader,
                                                        alignment: .topLeading)]
        }
        
        return section
    }
    
    func normal() -> NSCollectionLayoutSection {
        let item = NSCollectionLayoutItem(layoutSize:
                .init(widthDimension: .fractionalWidth(1),
                      heightDimension: .fractionalHeight(1)))
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize:
                .init(widthDimension: .fractionalWidth(0),
                      heightDimension: .fractionalHeight(0)),
                                                       subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        return section
    }
    
}
