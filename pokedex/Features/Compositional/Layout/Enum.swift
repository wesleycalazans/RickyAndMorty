//
//  Enum.swift
//  pokedex
//
//  Created by Wesley Calazans on 15/08/23.
//

import UIKit

enum LayoutType {
    case filters
    case reusable
    case normal
    
    func getLayout(withHeader: Bool = true) -> NSCollectionLayoutSection {
        
        switch self {
        case .filters:
            return AppLayout.shared.filters()
            
        case .reusable:
            return AppLayout.shared.reusable(withHeader: true)
            
        case .normal:
            return AppLayout.shared.normal()
        }
    }
}
